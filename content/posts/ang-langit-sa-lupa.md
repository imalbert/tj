---
title: "Ang Langit sa Lupa"
date: 2022-03-19T00:00:00+08:00
draft: false
---

Sabi nila'y ang Langit at Lupa
Ay nilikha ni Bathala.
Ano't ang naghahari ay
Impiyerno sa lupa?
Ang mga demonyo ang
Nagpapasasa sa yamang
Lupa at mga likha?

Mamamayan ng daigdig
lugmok sa kahirapan
Lupaypay sa kawalan
Habang ang iilan
Sakim sa karangyaan
Apaw sa kapangyarihan
Busog sa nakaw na yaman.

Mamamayan ng daigdig
Baliktarin ang kalagayan
Nasa inyo ang tunay na
lakas na baguhin iyan
Pairalin ang wasto
Ipaglaban ang karapatan
Baguhin ang mundo

Ang Langit sa Lupa,
Ay uring manggagawa at
Buong Sangkatauhan
Ang tunay na lilikha.
Sa huli, iiral ang tunay
Na hustisya, kasaganahan
at pag-unlad ng daigdigang Lipunan.

Tay Jun, 19 Mar 22
